package com.helen.drinks.data.mapper

import com.helen.drinks.data.model.Category
import com.helen.drinks.data.response.CategoryResponse
import org.koin.core.KoinComponent

class CategoryMapper : KoinComponent {

    fun transform(categoryResponse: CategoryResponse) = categoryResponse.let {
        mutableListOf<com.helen.drinks.domain.model.Category>().apply {
            it.drinks.forEach {
                add(transform(it))
            }
        }.toList()
    }

    fun transform(category: Category) = category.run {
        com.helen.drinks.domain.model.Category(
            strCategory
        )
    }
}