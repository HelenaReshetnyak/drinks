package com.helen.drinks.data.response

import com.helen.drinks.data.model.Category

data class CategoryResponse(
    val drinks: List<Category>
)