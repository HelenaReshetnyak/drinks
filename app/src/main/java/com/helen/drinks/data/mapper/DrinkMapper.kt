package com.helen.drinks.data.mapper

import com.helen.drinks.data.model.Drink
import com.helen.drinks.data.response.DrinksResponse
import com.helen.drinks.domain.model.DrinksByCategory
import org.koin.core.KoinComponent

class DrinkMapper : KoinComponent {

    fun transform(category: String, drinksResponse: DrinksResponse) =
        DrinksByCategory(
            category,
            transform(drinksResponse)
        )

    fun transform(drinksResponse: DrinksResponse) = drinksResponse.let {
        mutableListOf<com.helen.drinks.domain.model.Drink>().apply {
            it.drinks.forEach {
                add(transform(it))
            }
        }.toList()
    }

    fun transform(drink: Drink) = drink.run {
        com.helen.drinks.domain.model.Drink(
            idDrink,
            strDrink,
            strDrinkThumb
        )
    }
}