package com.helen.drinks.data.model

data class Category(
    val strCategory: String
)