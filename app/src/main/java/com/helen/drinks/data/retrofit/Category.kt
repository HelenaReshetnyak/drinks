package com.helen.drinks.data.retrofit

import com.helen.drinks.data.response.CategoryResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface Category {

    @GET("/api/json/v1/1/list.php")
    suspend fun getCategory(
        @Query("c") type: String
    ): CategoryResponse
}
