package com.helen.drinks.data.response

import com.helen.drinks.data.model.Drink

data class DrinksResponse(
    val drinks: List<Drink>
)