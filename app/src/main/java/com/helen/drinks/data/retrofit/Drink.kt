package com.helen.drinks.data.retrofit

import com.helen.drinks.data.response.DrinksResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface Drink {

    @GET("/api/json/v1/1/filter.php")
    suspend fun getDrinks(
        @Query("c") category: String
    ): DrinksResponse
}