package com.helen.drinks.data.storage

import com.helen.drinks.data.mapper.DrinkMapper
import com.helen.drinks.domain.model.Category
import com.helen.drinks.domain.model.DrinksByCategory
import com.helen.drinks.domain.repository.DrinkRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

class DrinkStorage : DrinkRepository, KoinComponent {

    private val drink by inject<com.helen.drinks.data.retrofit.Drink>()
    private val drinkMapper by inject<DrinkMapper>()

    override suspend fun getDrinks(category: Category): DrinksByCategory {
        return drink.getDrinks(category.name).run {
            drinkMapper.transform(category.name, this)
        }
    }

}