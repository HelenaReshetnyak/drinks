package com.helen.drinks.data.model

data class Drink(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)