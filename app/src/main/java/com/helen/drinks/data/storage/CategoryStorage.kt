package com.helen.drinks.data.storage

import com.helen.drinks.data.mapper.CategoryMapper
import com.helen.drinks.domain.model.Category
import com.helen.drinks.domain.repository.CategoryRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

class CategoryStorage : CategoryRepository, KoinComponent {

    private val category by inject<com.helen.drinks.data.retrofit.Category>()
    private val categoryMapper by inject<CategoryMapper>()

    override suspend fun getCategory(): List<Category> {
        return category.getCategory("list").run {
            categoryMapper.transform(this)
        }
    }
}