package com.helen.drinks.di

import com.helen.drinks.data.retrofit.Category
import com.helen.drinks.data.retrofit.Drink
import com.helen.drinks.di.base.DIModule
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.BuildConfig
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiModule : DIModule {

    private val baseUrl = "https://www.thecocktaildb.com"
    private val apiModule = module {
        single {
            val builder = OkHttpClient()
                .newBuilder()
                .followRedirects(false)

            if (BuildConfig.DEBUG) {
                builder.addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
            }

            builder.build()
        }

        single<Drink> {
            Retrofit.Builder()
                .client(get())
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Drink::class.java)
        }

        single<Category> {
            Retrofit.Builder()
                .client(get())
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Category::class.java)
        }
    }

    override fun getDependencies(): Module = apiModule
}