package com.helen.drinks.di

import com.helen.drinks.data.mapper.CategoryMapper
import com.helen.drinks.data.mapper.DrinkMapper
import com.helen.drinks.di.base.DIModule
import org.koin.dsl.module

object MapperModule : DIModule {

    override fun getDependencies() = module {
        single { DrinkMapper() }
        single { CategoryMapper() }

        single { com.helen.drinks.ui.mapper.CategoryMapper() }
        single { com.helen.drinks.ui.mapper.DrinkMapper() }
    }
}