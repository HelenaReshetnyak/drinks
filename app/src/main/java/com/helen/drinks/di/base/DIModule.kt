package com.helen.drinks.di.base

import org.koin.core.module.Module

interface DIModule {
    fun getDependencies(): Module
}