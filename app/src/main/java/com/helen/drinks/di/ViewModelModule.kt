package com.helen.drinks.di

import com.helen.drinks.di.base.DIModule
import com.helen.drinks.ui.view.main.drinks_list.DrinksViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object ViewModelModule : DIModule {

    override fun getDependencies() = module {
        viewModel { DrinksViewModel() }
    }
}