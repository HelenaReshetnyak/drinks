package com.helen.drinks.di

import com.helen.drinks.data.storage.CategoryStorage
import com.helen.drinks.data.storage.DrinkStorage
import com.helen.drinks.di.base.DIModule
import com.helen.drinks.domain.repository.CategoryRepository
import com.helen.drinks.domain.repository.DrinkRepository
import org.koin.dsl.module

object RepositoryModule : DIModule {

    override fun getDependencies() = module {
        single<DrinkRepository> { DrinkStorage() }
        single<CategoryRepository> { CategoryStorage() }
    }
}