package com.helen.drinks

import android.app.Application
import com.helen.drinks.di.ApiModule
import com.helen.drinks.di.MapperModule
import com.helen.drinks.di.RepositoryModule
import com.helen.drinks.di.ViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class Drinks : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@Drinks)
            modules(
                listOf(
                    ApiModule.getDependencies(),
                    RepositoryModule.getDependencies(),
                    MapperModule.getDependencies(),
                    ViewModelModule.getDependencies()
                )
            )
        }
    }
}