package com.helen.drinks.domain.model

data class Category(
    val name: String
)