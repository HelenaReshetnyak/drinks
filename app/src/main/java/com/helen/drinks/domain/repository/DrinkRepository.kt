package com.helen.drinks.domain.repository

import com.helen.drinks.domain.model.Category
import com.helen.drinks.domain.model.DrinksByCategory

interface DrinkRepository {
    suspend fun getDrinks(category: Category): DrinksByCategory
}