package com.helen.drinks.domain.model

data class Drink(
    val id: String,
    val name: String,
    val image: String
)