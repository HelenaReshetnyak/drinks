package com.helen.drinks.domain.repository

import com.helen.drinks.domain.model.Category

interface CategoryRepository {
    suspend fun getCategory(): List<Category>
}