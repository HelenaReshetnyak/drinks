package com.helen.drinks.domain.model

data class DrinksByCategory(
    val category: String,
    val drinks: List<Drink>
)