package com.helen.drinks.ui.view.main.filter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.helen.drinks.ui.model.CategoryView
import kotlinx.android.synthetic.main.item_filter_list.view.*


class FilterAdapter(
    private val layoutId: Int,
    private var filters: List<CategoryView>,
    private val onFilterItemClickListener: OnFilterItemClickListener
) : RecyclerView.Adapter<FilterAdapter.FilterViewHolder>() {


    fun updateList(newList: List<CategoryView>) {
        filters = newList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterViewHolder {
        return LayoutInflater.from(parent.context).inflate(layoutId, parent, false).run {
            FilterViewHolder(this)
        }
    }

    override fun onBindViewHolder(holder: FilterViewHolder, position: Int) =
        holder.bind(filters[position])

    override fun getItemCount(): Int = filters.size

    inner class FilterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(category: CategoryView) {
            itemView.run {
                if (category.isSelected) isSelectedImage.visibility = View.VISIBLE
                else isSelectedImage.visibility = View.GONE

                nameFilter.text = category.name

                setOnClickListener {
                    category.isSelected = category.isSelected.not()
                    onFilterItemClickListener.onFilterItemClick(category)
                    notifyDataSetChanged()
                }
            }
        }
    }

    interface OnFilterItemClickListener {
        fun onFilterItemClick(filter: CategoryView)
    }

}