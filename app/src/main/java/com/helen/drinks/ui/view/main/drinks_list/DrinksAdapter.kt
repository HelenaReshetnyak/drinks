package com.helen.drinks.ui.view.main.drinks_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.helen.drinks.R
import com.helen.drinks.domain.model.Drink
import com.helen.drinks.ui.extension.EMPTY
import com.helen.drinks.ui.model.AdapterItem
import kotlinx.android.synthetic.main.bottom_drink_list.view.*
import kotlinx.android.synthetic.main.item_drink_list.view.*
import kotlinx.android.synthetic.main.item_header_drink_list.view.*

class DrinksAdapter(
    private var list: List<AdapterItem<String>>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isEndList: Boolean = false

    fun updateIsEndList(isEnd: Boolean) {
        this.isEndList = isEnd
    }

    fun updateList(newList: List<AdapterItem<String>>) {
        list = newList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER_VIEW_TYPE -> HeadersViewHolder(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_header_drink_list, parent, false)
            )
            ITEM_VIEW_TYPE -> DrinksViewHolder(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.item_drink_list, parent, false)
            )
            else -> BottomViewHolder(
                LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.bottom_drink_list, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeadersViewHolder -> holder.bind(list[position].value ?: String.EMPTY)
            is DrinksViewHolder -> holder.bind(list[position].value?.run {
                Gson().fromJson(this, Drink::class.java)
            })
            is BottomViewHolder -> holder.bind()
            else -> { /* Nothing to do here */
            }
        }
    }

    override fun getItemCount(): Int = list.size + 1

    override fun getItemViewType(position: Int) =
        when (position) {
            list.size -> BOTTOM_VIEW_TYPE
            else -> list[position].viewType
        }

    inner class DrinksViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(drinksByCategory: Drink?) {
            itemView.run {
                drinksByCategory?.let { drink ->
                    Glide
                        .with(drinkImage)
                        .load(drink.image)
                        .into(drinkImage)

                    drinkName.text = drink.name
                }
            }
        }
    }

    inner class HeadersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(category: String) {
            itemView.run {
                categoryName.text = category
            }
        }
    }

    inner class BottomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind() {
            itemView.run {
                if (isEndList) {
                    progress.visibility = View.GONE
                    endList.visibility = View.VISIBLE
                } else {
                    progress.visibility = View.VISIBLE
                    endList.visibility = View.GONE
                }
            }
        }
    }

    companion object {
        const val HEADER_VIEW_TYPE = 0
        const val ITEM_VIEW_TYPE = 1
        const val BOTTOM_VIEW_TYPE = 2
    }

    interface DrinkListListener {
        fun onVisible()
    }
}