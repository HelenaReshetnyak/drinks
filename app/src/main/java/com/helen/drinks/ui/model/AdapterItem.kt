package com.helen.drinks.ui.model

data class AdapterItem<out T>(val value: T?, val viewType: Int)
