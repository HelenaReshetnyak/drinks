package com.helen.drinks.ui.mapper

import com.google.gson.Gson
import com.helen.drinks.domain.model.DrinksByCategory
import com.helen.drinks.ui.model.AdapterItem
import com.helen.drinks.ui.view.main.drinks_list.DrinksAdapter
import org.koin.core.KoinComponent

class DrinkMapper : KoinComponent {

    fun transform(drinksByCategory: List<DrinksByCategory>) =
        mutableListOf<AdapterItem<String>>().apply {
            drinksByCategory.forEach { drinksByCategory ->
                transform(drinksByCategory).forEach { item ->
                    add(item)
                }
            }
        }.toList()


    private fun transform(drinksByCategory: DrinksByCategory) =
        mutableListOf<AdapterItem<String>>().apply {

            add(AdapterItem(drinksByCategory.category, DrinksAdapter.HEADER_VIEW_TYPE))

            drinksByCategory.drinks.forEach {
                add(AdapterItem(Gson().toJson(it), DrinksAdapter.ITEM_VIEW_TYPE))
            }

        }.toList()


}