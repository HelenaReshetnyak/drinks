package com.helen.drinks.ui.base

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.DrawableRes
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.helen.drinks.R
import kotlinx.android.synthetic.main.toolbar_title.*

abstract class BaseToolbarActivity : AppCompatActivity() {

    abstract val contentView: Int

    private var isNavigationIconListener = false

    private var onNavigationIconClicked: () -> Unit = {}

    @StringRes
    protected open val screenTitleRes: Int = 0

    protected open val screenTitle: String = ""

    @DrawableRes
    protected open val toolbarNavigationIcon: Int = 0

    @MenuRes
    protected open val optionsMenu: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(contentView)

        (findViewById(R.id.toolbar) as? Toolbar)?.let {
            setSupportActionBar(it)

            supportActionBar?.run {
                setDisplayHomeAsUpEnabled(false)
                setDisplayShowTitleEnabled(false)
                initToolbarParams()
            }
        } ?: throw Exception("Activity must contain AppBarLayout")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (optionsMenu != 0)
            menuInflater.inflate(optionsMenu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (isNavigationIconListener)
                    onNavigationIconClicked()
                else onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun changeToolbarTitle(@StringRes titleId: Int) {
        changeToolbarTitle(getString(titleId))
    }

    fun changeToolbarTitle(title: String) {
        toolbarTitle.text = title
    }

    fun changeNavigationIcon(@DrawableRes navigationIcon: Int?) {
        (findViewById(R.id.toolbar) as? Toolbar)?.let { toolbar ->
            navigationIcon
                ?.run { toolbar.setNavigationIcon(this) }
                ?: run { toolbar.navigationIcon = null }
        }
    }

    fun setOnNavigationIconClickListener(clickListener: () -> Unit) {
        this.onNavigationIconClicked = clickListener
        isNavigationIconListener = true
    }

    private fun initToolbarParams() {
        if (screenTitleRes != 0)
            toolbarTitle.text = getString(screenTitleRes)

        if (screenTitle.isNotEmpty()) {
            toolbarTitle.text = screenTitle
        }

        if (toolbarNavigationIcon != 0)
            changeNavigationIcon(toolbarNavigationIcon)
    }

}