package com.helen.drinks.ui.mapper

import com.helen.drinks.domain.model.Category
import com.helen.drinks.ui.model.CategoryView
import org.koin.core.KoinComponent

class CategoryMapper : KoinComponent {

    fun transform(categories: List<Category>) = categories.let {
        mutableListOf<CategoryView>().apply {
            it.forEach {
                add(transform(it))
            }
        }.toList()
    }

    fun transform(category: Category) = category.run {
        CategoryView(
            name,
            false
        )
    }

    fun updateSelectedCategories(categories: List<CategoryView>) =
        mutableListOf<Category>().apply {
            categories.forEach { category ->
                if (category.isSelected)
                    add(Category(category.name))
            }
        }.toList()

    fun transformListCategoryViewToCategory(categories: List<CategoryView>) = categories.let {
        mutableListOf<Category>().apply {
            it.forEach {
                add(transform(it))
            }
        }.toList()
    }

    fun transform(categoryView: CategoryView) = categoryView.run {
        Category(
            name
        )
    }
}