package com.helen.drinks.ui.view.main.drinks_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.helen.drinks.R
import com.helen.drinks.ui.base.BaseToolbarFragment
import com.helen.drinks.ui.mapper.DrinkMapper
import kotlinx.android.synthetic.main.fragment_drinks.*
import kotlinx.android.synthetic.main.toolbar_title.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DrinksFragment : BaseToolbarFragment() {

    override val screenTitleRes = R.string.app_name
    override val optionsMenu: Int = R.menu.filter_menu

    private val drinkMapper: DrinkMapper by inject()
    private val drinksViewModel: DrinksViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_drinks, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        bindUi()
    }

    private fun initUi() {

        drinksRecyclerView.adapter = DrinksAdapter(emptyList())

        drinksRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                    .run {
                        if (this == recyclerView.adapter?.itemCount?.minus(1)) {
                            drinksViewModel.updateNumberCategory()
                        }

                    }
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }

    private fun bindUi() {
        drinksViewModel.observeSelectedCategory().observe(viewLifecycleOwner, {
            drinksViewModel.newNumberCategory(it)
        })

        drinksViewModel.observeDrinksList().observe(viewLifecycleOwner, {
            (drinksRecyclerView.adapter as DrinksAdapter).updateList(drinkMapper.transform(it))
            drinksRecyclerView.adapter?.notifyDataSetChanged()
        })

        drinksViewModel.observeNumberCategory().observe(viewLifecycleOwner, {
            drinksViewModel.getDrinks()
            it?.run {
                if (first >= second)
                    (drinksRecyclerView.adapter as DrinksAdapter).updateIsEndList(true)
                else
                    (drinksRecyclerView.adapter as DrinksAdapter).updateIsEndList(false)

                drinksRecyclerView.adapter?.notifyDataSetChanged()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        requireActivity().toolbar.navigationIcon = null
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.filter -> {
                findNavController().navigate(R.id.main_drinksFragment_to_filterFragment)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}