package com.helen.drinks.ui.model

data class CategoryView(
    val name: String,
    var isSelected: Boolean
)