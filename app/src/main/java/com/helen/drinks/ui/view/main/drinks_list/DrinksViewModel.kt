package com.helen.drinks.ui.view.main.drinks_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.helen.drinks.domain.model.Category
import com.helen.drinks.domain.model.DrinksByCategory
import com.helen.drinks.domain.repository.CategoryRepository
import com.helen.drinks.domain.repository.DrinkRepository
import com.helen.drinks.ui.base.BaseViewModel
import com.helen.drinks.ui.mapper.CategoryMapper
import com.helen.drinks.ui.model.CategoryView
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

class DrinksViewModel : BaseViewModel(), KoinComponent {

    private val drinkRepository: DrinkRepository by inject()
    private val categoryRepository: CategoryRepository by inject()
    private val categoryMapper: CategoryMapper by inject()

    private val categories: MutableLiveData<List<CategoryView>> = MutableLiveData()
    private val drinksList: MutableLiveData<List<DrinksByCategory>> = MutableLiveData(emptyList())
    private val onError: MutableLiveData<Exception> = MutableLiveData()
    private val numberCategory: MutableLiveData<Pair<Int, Int>> = MutableLiveData()
    private var list: List<DrinksByCategory> = emptyList()
    private val selectedCategories: MutableLiveData<List<Category>> = MutableLiveData()

    fun getCategory() = launch {
        try {
            categoryRepository.getCategory().run {
                categories.postValue(categoryMapper.transform(this))
                selectedCategories.postValue(this)
            }
        } catch (ex: Exception) {
            onError.postValue(ex)
        }
    }

    fun getDrinks() = launch {
        try {
            selectedCategories.value?.run {
                numberCategory.value?.let { number ->
                    drinkRepository.getDrinks(this[number.first]).let { newDrinks ->
                        drinksList.postValue(updateList(newDrinks, number.first))
                    }
                }
            }
        } catch (ex: Exception) {
            onError.postValue(ex)
        }
    }

    fun newNumberCategory(list: List<Category>) {
        numberCategory.value = Pair(0, list.size)
    }

    private fun updateList(newItem: DrinksByCategory, numberCategory: Int): List<DrinksByCategory> {
        list = if (numberCategory == 0) listOf(newItem)
        else {
            mutableListOf<DrinksByCategory>().apply {
                list.forEach {
                    add(it)
                }
                add(newItem)
            }
        }.toList()
        return list
    }

    fun revertCategories() {
        categories.value?.forEach {
            it.isSelected = false
        }

        val categories = categories.value ?: emptyList()
        val selectedCategories = selectedCategories.value ?: emptyList()

        if (categories.size != selectedCategories.size) {
            categories.forEach { category ->
                selectedCategories.forEach { selectedCategory ->
                    if (category.name == selectedCategory.name) category.isSelected = true
                }
            }
        }
    }

    fun updateCategories(categoryView: CategoryView) {
        categories.value?.forEach {
            if (categoryView.name == it.name) it.isSelected = categoryView.isSelected
        }
    }

    fun updateSelectedCategories() {
        categories.value?.run {
            val newSelectedList = categoryMapper.updateSelectedCategories(this)
            if (newSelectedList.isEmpty())
                selectedCategories.value = categoryMapper.transformListCategoryViewToCategory(this)
            else selectedCategories.value = newSelectedList
        }
    }

    fun updateNumberCategory() {
        numberCategory.value?.run {
            numberCategory.value = Pair(this.first + 1, this.second)
        }
    }

    fun observeNumberCategory(): LiveData<Pair<Int, Int>> = numberCategory
    fun observeCategory(): LiveData<List<CategoryView>> = categories
    fun observeSelectedCategory(): LiveData<List<Category>> = selectedCategories
    fun observeDrinksList(): LiveData<List<DrinksByCategory>> = drinksList
    fun observeError(): LiveData<Exception> = onError

}