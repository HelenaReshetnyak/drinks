package com.helen.drinks.ui.view.main.filter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.helen.drinks.R
import com.helen.drinks.ui.base.BaseToolbarFragment
import com.helen.drinks.ui.model.CategoryView
import com.helen.drinks.ui.view.main.drinks_list.DrinksViewModel
import kotlinx.android.synthetic.main.fragment_filter.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class FilterFragment : BaseToolbarFragment() {

    override val screenTitleRes = R.string.filter
    override val toolbarNavigationIcon = R.drawable.vector_ic_back

    private val drinksViewModel: DrinksViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        bindUi()
    }

    private fun initUi() {
        setOnNavigationIconClickListener {
            drinksViewModel.revertCategories()
            findNavController().popBackStack()
        }

        filterRecyclerView.adapter = FilterAdapter(
            R.layout.item_filter_list,
            emptyList(),
            object : FilterAdapter.OnFilterItemClickListener {
                override fun onFilterItemClick(filter: CategoryView) {
                    drinksViewModel.updateCategories(filter)
                }
            }
        )

        applyButton.setOnClickListener {
            drinksViewModel.updateSelectedCategories()
            findNavController().popBackStack()
        }
    }

    private fun bindUi() {
        drinksViewModel.observeCategory().observe(viewLifecycleOwner, {
            (filterRecyclerView.adapter as FilterAdapter).updateList(it)
            filterRecyclerView.adapter?.notifyDataSetChanged()
        })
    }

}