package com.helen.drinks.ui.view.main

import android.os.Bundle
import androidx.activity.viewModels
import com.helen.drinks.R
import com.helen.drinks.ui.base.BaseToolbarActivity
import com.helen.drinks.ui.view.main.drinks_list.DrinksViewModel

class MainActivity : BaseToolbarActivity() {

    override val contentView = R.layout.activity_main

    private val drinksViewModel: DrinksViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        drinksViewModel.getCategory()
    }

}